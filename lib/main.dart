import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_2/app_state.dart';
import 'package:redux_2/reducer.dart';
import 'package:redux_2/setting_page.dart';

void main() {
  final _initialState = AppState(silderFontSize: 0);
  final Store<AppState> _store = Store(reducer, initialState: _initialState);
  runApp(MyApp(
    store: _store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
        store: store,
        child: const MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Settings(),
        ));
  }
}
