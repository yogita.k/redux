class AppState {
  bool? italic;
  bool? bold;
  double? silderFontSize;

  AppState(
      {this.italic = false, this.bold = false, required this.silderFontSize});
  AppState.fromAppState(AppState appState){
    italic=appState.italic;
    bold=appState.bold;
    silderFontSize=appState.silderFontSize;
  }
  double get viewFontSize =>  silderFontSize! * 100;
}
