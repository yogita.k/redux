import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_2/action.dart';
import 'package:redux_2/app_state.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  TextStyle getStyle([bool isBold = false, bool isItalic = false]) {
    return TextStyle(
      fontSize: 40,
      fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
      fontStyle: isItalic ? FontStyle.italic : FontStyle.normal,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Settings page")),
      body: StoreConnector<AppState, AppState>(
        builder: (context, state) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 40),
                child: Center(
                    child: Text(
                  "font size is ${state.viewFontSize.toInt()}",
                  style: TextStyle(fontSize: 36),
                )),
              ),
              Slider(
                value: state.silderFontSize!,
                onChanged: (value) {
                  StoreProvider.of<AppState>(context).dispatch(FontSize(value));
                },
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 8),
                child: Row(children: [
                  Checkbox(
                    value: state.bold,
                    onChanged: (value) {
                      try {
                        StoreProvider.of<AppState>(context)
                            .dispatch(Bold(value!));
                      } catch (e) {
                        print(e);
                      }
                    },
                  ),
                  Text(
                    "bold",
                    style: getStyle(state.bold!,false),
                  )
                ]),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 8),
                child: Row(children: [
                  Checkbox(
                    value: state.italic,
                    onChanged: (value) {
                      try {
                        StoreProvider.of<AppState>(context)
                            .dispatch(Italic(value!));
                      } catch (e) {
                        print(e);
                      }
                    },
                  ),
                  Text(
                    "italic",
                    style: getStyle(false,state.italic!),
                  )
                ]),
              )
            ],
          );
        },
        converter: (store) => store.state,
      ),
    );
  }
}
