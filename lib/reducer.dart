import 'package:redux_2/action.dart';
import 'package:redux_2/app_state.dart';

AppState reducer(AppState prevState, dynamic action) {
  AppState newState = AppState.fromAppState(prevState);
  if (action is FontSize) {
    newState.silderFontSize = action.playload;
  } else if (action is Bold) {
    newState.bold = action.playload;
  } else if (action is Italic) {
    newState.italic = action.playload;
  }
  return newState;
}
